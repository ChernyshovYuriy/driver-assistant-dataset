import argparse
import csv
from os.path import dirname, abspath

# Parse arguments provided.
ap = argparse.ArgumentParser()
ap.add_argument(
    "-f", "--f", type=str, default="",
    help="Path to C++'s project road_sign.h file"
)

args = vars(ap.parse_args())

# Absolute path to this Project's root.
project_root = dirname(abspath(__file__))
# Path to the road_sign.h
cpp_file_path = args["f"]
# Number of Road Signs
signs_number = 0

print('road_sign.h file path:', cpp_file_path)


# Read CSV file and return list of tuples
def read_csv():
    global project_root, signs_number
    csv_file_path = project_root + '/docs/road_signs.csv'
    print('Road Signs CSV file path:', csv_file_path)
    result = []
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            folder_id = row[0].strip()
            enum_item = row[1].strip()
            signs_number += 1

            item = (folder_id, enum_item)
            result.append(item)
    return result


# Create C++ header file which describes Road Sings
def create_cpp_header_file(data_input):
    global signs_number, cpp_file_path

    file = open(cpp_file_path, "w")
    file.write("//\n")
    file.write("// Generated by DriverAssistantDataset's python script\n")
    file.write("//\n\n")
    file.write("#ifndef EYEONTHEROADCPP_ROADSIGN_H\n")
    file.write("#define EYEONTHEROADCPP_ROADSIGN_H\n\n")
    file.write("using namespace std;\n\n")
    file.write("static const int SIGNS_NUMBER = " + str(signs_number) + ";\n\n")
    file.write("static const string RoadSignLabel[SIGNS_NUMBER] {\n")
    for pair in data_input:
        file.write("    \"" + str(pair[1]) + "\",\n")
    file.write("};\n\n")
    file.write("enum RoadSign {\n")
    for pair in data_input:
        file.write("    " + str(pair[1]) + ",\n")
    file.write("};\n\n")
    file.write("#endif //EYEONTHEROADCPP_ROADSIGN_H\n")
    file.close()


# Read data from CSV file
data = read_csv()
#
create_cpp_header_file(data)

print("Road Signs number is", signs_number)
