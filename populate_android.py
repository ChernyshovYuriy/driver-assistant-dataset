import argparse
import csv
from os import listdir, makedirs
from os.path import isfile, join, exists, dirname, abspath
from shutil import copyfile

# Parse arguments provided.
ap = argparse.ArgumentParser()
ap.add_argument(
    "-arr", "--arr", type=str, default="android_raw_resources",
    help="Path to Android's project raw resources"
)
ap.add_argument(
    "-arrt", "--arrt", type=str, default="android_raw_resources_test",
    help="Path to Android's project test raw resources"
)
args = vars(ap.parse_args())

# Path to the directory with Road Signs images to use for training.
training_signs_path = "training_images/signs"
testing_noise_imgs_path = "testing_images/noise"
# Name of the file to use for Image-Label mapping.
training_mapping_file_name = "training_map.csv"
# Path to the raw resources of the Android project.
android_raw_resources_path = args["arr"]
# Path to the raw resources of the Android test project.
android_test_raw_resources_path = args["arrt"]
# Absolute path to this Project's root.
project_root = dirname(abspath(__file__))


# Create directory if there is no such.
def create_dir_if_needed(dir_name):
    if not exists(dir_name):
        makedirs(dir_name)


# Populates sign images from the training set into the Android project.
# Create name for each image to match next scheme "trng_img_<label_id>_<img_name>", where
# <label_id> is Id of the Label in the global Road Signs mapping and <img_name> is a name of each image in the set.
# Also, create a mapping file with "csv" format to map each image to corresponded Road Sign label.
def populate_training_android():
    global project_root, android_raw_resources_path, android_test_raw_resources_path, training_signs_path, training_mapping_file_name
    print("Populate road signs to Android project, path:", android_raw_resources_path)
    print("Populate road signs to Android test project, path:", android_test_raw_resources_path)
    """
    Create Android raw directory if needed.
    """
    create_dir_if_needed(android_raw_resources_path)
    create_dir_if_needed(android_test_raw_resources_path)
    """
    Map of the image and its Label in the Road Signs labels set.
    """
    imgs_mapping = dict()
    """
    List of all directories with Road Signs in the training folder.
    """
    directories = [directory for directory in listdir(training_signs_path) if
                   not isfile(join(training_signs_path, directory))]
    """
    Iterate over directories founded.
    """
    for directory in directories:
        """
        Get list of all files in the directory.
        """
        files = [file for file in listdir(join(training_signs_path, directory)) if
                 isfile(join(training_signs_path, directory, file))]
        print("  visiting sign directory", directory)
        """
        Iterate over files founded.
        """
        for file in files:
            """
            Create a name of the file for Android.
            """
            android_file_name = "trng_img_" + directory + "_" + file
            """
            Create a path to copy to a file.
            """
            android_file_path = join(android_raw_resources_path, android_file_name)
            android_test_file_path = join(android_test_raw_resources_path, android_file_name)
            """
            Copy file to Android's raw resources.
            """
            copyfile(join(project_root, training_signs_path, directory, file), android_file_path)
            copyfile(join(project_root, training_signs_path, directory, file), android_test_file_path)
            """
            Associate a file (by its name) with the Road Sign Label Id.
            """
            imgs_mapping[android_file_name] = int(directory)
            print("     file", file, " to", android_file_path)
    """
    Create writer to save mapping to the file, location is Android's raw resources.
    """
    mapping_file_path = join(android_raw_resources_path, "android_" + training_mapping_file_name)
    mapping_file_test_path = join(android_test_raw_resources_path, "android_" + training_mapping_file_name)
    writer = csv.writer(open(mapping_file_path, "w"))
    for key, val in imgs_mapping.items():
        writer.writerow([key, val])
    writer = csv.writer(open(mapping_file_test_path, "w"))
    for key, val in imgs_mapping.items():
        writer.writerow([key, val])
    print("Android training map saved into:", mapping_file_path)
    print("Android training test map saved into:", mapping_file_test_path)


def populate_testing_android():
    global project_root, android_test_raw_resources_path, testing_noise_imgs_path
    print("Populate 'noise images' to Android test project, path:", android_test_raw_resources_path)
    """
    Create Android raw directory if needed.
    """
    create_dir_if_needed(android_test_raw_resources_path)
    """
    Get list of all noise images in the directory.
    """
    files = [file for file in listdir(join(testing_noise_imgs_path, "android")) if
             isfile(join(testing_noise_imgs_path, "android", file))]
    """
    Iterate over files founded.
    """
    for file in files:
        """
        Create a path to copy to a file.
        """
        android_test_file_path = join(android_test_raw_resources_path, file)
        """
        Copy file to Android's raw resources.
        """
        copyfile(join(project_root, testing_noise_imgs_path, "android", file), android_test_file_path)
        print("  from:", join(project_root, testing_noise_imgs_path, "android", file))
        print("    to:", android_test_file_path)


# Populate Android resources.
populate_training_android()

# Populate Android testing resources.
populate_testing_android()
