import argparse
import os
import sys
import urllib.request
from os.path import dirname, abspath

import cv2

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

ap = argparse.ArgumentParser()
ap.add_argument("-url", "--url", type=str,
                help="URL of the images set obtained from http://www.image-net.org")
ap.add_argument("-dst", "--destination", type=str,
                help="Path to the directory to download images to. "
                     "Must be absolute path.")
args = vars(ap.parse_args())

url = args["url"]
dir_name = args["destination"]
print("Downloading images from:", url)
print("Downloading images to  :", dir_name)


def store_raw_images():
    global dir_name
    image_urls = urllib.request.urlopen(url).read().decode()
    pic_num = 1

    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

    for i in image_urls.split('\n'):
        try:
            print(i)
            urllib.request.urlretrieve(i, dir_name + "/" + str(pic_num) + ".jpg")
            img = cv2.imread(dir_name + "/" + str(pic_num) + ".jpg", cv2.IMREAD_GRAYSCALE)
            resized_image = cv2.resize(img, (300, 300))
            file_path = dir_name + "/" + str(pic_num) + ".jpg"
            cv2.imwrite(file_path, resized_image)
            pic_num += 1

            print("Image '", file_path, "'")

        except Exception as e:
            print(str(e))


store_raw_images()

print("Images downloaded")