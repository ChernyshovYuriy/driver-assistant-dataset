# README #

This repository contains Road Sign images that are used for training of the image key-points descriptors matcher.
Dataset is used by [Driver Assistant Android](https://bitbucket.org/ChernyshovYuriy/driver-assistant-android) and [Driver Assistant C++](https://bitbucket.org/ChernyshovYuriy/driver-assistant-c) projects.

Road Signs are labeled. Each Road Sign corresponds to concrete Unique Identifier (Label), description is provided in the **RoadSignLabel.txt** file locating in docs folder of this project.

There is **main.py** file which is responsible for files distribution to the [Driver Assistant Android](https://bitbucket.org/ChernyshovYuriy/driver-assistant-android) project. Because Android does not allow to have folders in its **raw** resources, images distributing with specific naming rule:

```
#!python

android_file_name = "trng_img_" + directory + "_" + file
```
where "directory" is a serial number of current directory, e.g. 0, 1, 2, 3 ... .

Each directory number corresponds to Road Sign Label id. For instance, **No Left Sign** corresponds to folder named **0**, **Stop Sign** to folder named **3**, etc ... . In this way of images distribution there is a set of images that corresponds to each Road Sign Label.

Look at the parameters accepting by **main.py** in order to distribute them.

[Driver Assistant C++](https://bitbucket.org/ChernyshovYuriy/driver-assistant-c) project may use this dataset directly by specifying path to the **Driver Assistant Dataset/training_images/signs/**


### Who do I talk to? ###

Project owner and admin - Yurii Chernyshov

E-mail : chernyshov.yuriy@gmail.com

LinkedIn: https://www.linkedin.com/pub/yuriy-chernyshov/1b/622/270