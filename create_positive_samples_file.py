import os

import cv2
import numpy as np

src = "speed_limit_na/positive_samples"
data_file_name = "speed_limit_na/info.dat"

print("Create positive samples file from '", src, "'")

isfile = os.path.isfile(data_file_name)
if isfile:
    os.remove(data_file_name)

img_current_count = 0
img_total = len(os.listdir(src))
for filename in os.listdir(src):
    if filename.endswith(".jpg") or filename.endswith(".jpeg"):
        # print(os.path.join(directory, filename))
        img = cv2.imread(os.path.join(src, filename))
        height = np.size(img, 0)
        width = np.size(img, 1)
        line = "positive_samples/" + filename + " 1 0 0 " + str(width) + " " + str(height) + "\n"
        with open(data_file_name, "a") as data_file:
            data_file.write(line)
        img_current_count += 1
        print("Image", img_current_count, "of", img_total, "processed")
    else:
        print("File is not JPEG image:", filename)
        print("Image", img_current_count, "of", img_total, "skipped")

print("Positive samples file created")