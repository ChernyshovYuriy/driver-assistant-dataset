import os

import cv2

src = "speed_limit_na/negative_samples"
width = 300
height = 300

counter = 0
for filename in os.listdir(src):
    if filename.endswith(".jpg") or filename.endswith(".jpeg"):
        try:
            """
            Read original image
            """
            img = cv2.imread(os.path.join(src, filename))
            """
            Remove original image from persistent storage (it is still in memory)
            """
            os.remove(os.path.join(src, filename))
            """
            Resize and convert it to gray scale
            """
            resized = cv2.resize(img, (width, height))
            resized = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
            """
            Provide unique name
            """
            filename = "bg_" + str(counter) + ".jpg"
            p = os.path.sep.join((src, filename))
            """
            Save new image to persistent storage
            """
            cv2.imwrite(p, resized)
            """
            Together with name prefix, this counter gives unique name within current directory
            """
            counter += 1
            print("Image", filename, "resize completed")
        except Exception as e:
            print("Image", filename, "resize exception:", e)
    else:
        print("File is not JPEG image:", filename)
        continue

print("All images resized")
