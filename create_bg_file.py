import os

src = "speed_limit_na/negative_samples"
data_file_name = "speed_limit_na/bg.txt"

print("Create positive samples file from '", src, "'")

isfile = os.path.isfile(data_file_name)
if isfile:
    os.remove(data_file_name)

img_current_count = 0
img_total = len(os.listdir(src))
for filename in os.listdir(src):
    if filename.endswith(".jpg") or filename.endswith(".jpeg"):
        line = "speed_limit_na/negative_samples/" + filename + "\n"
        with open(data_file_name, "a") as data_file:
            data_file.write(line)
        img_current_count += 1
        print("Image", img_current_count, "of", img_total, "processed")
    else:
        print("File is not JPEG image:", filename)
        print("Image", img_current_count, "of", img_total, "skipped")

print("Negatives text file created")
