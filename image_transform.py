import math
import os
import time
from random import randint
from threading import Lock

import cv2
import numpy
import numpy as np
from IPython.utils.py3compat import xrange
from PIL import Image
from numpy import random

# Provide here a custom path to the images which will be usd as positive ones.
src = "/home/yurii/Pictures/speed_limit_signs/pos"
# Destination directory.
dst = "../resources/speed_limit_na/positive_samples"
# It is OK now to use background images from "speed_limit_na" folder, there are about 1000 samples.
bg = "../resources/speed_limit_na/negative_samples"
# Number of mutations for each image from "src".
num = 1000
bg_images = []
width = 34
height = 50


def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0] * p1[0], -p2[0] * p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1] * p1[0], -p2[1] * p1[1]])

    A = numpy.matrix(matrix, dtype=numpy.float)
    B = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(A.T * A) * A.T, B)
    return numpy.array(res).reshape(8)


# Return color value depending on quadrant and saturation
def get_saturation(value, quadrant):
    if value > 223:
        return 255
    elif value > 159:
        if quadrant != 1:
            return 255

        return 0
    elif value > 95:
        if quadrant == 0 or quadrant == 3:
            return 255

        return 0
    elif value > 32:
        if quadrant == 1:
            return 255

        return 0
    else:
        return 0


# Create a dithered version of the image
def convert_dithering(image):
    # Get size
    img_width, img_height = image.size

    # Create new Image and a Pixel Map
    new = create_image(img_width, img_height)
    pixels = new.load()

    # Transform to half tones
    for i in range(0, img_width, 2):
        for j in range(0, img_height, 2):
            # Get Pixels
            p1 = get_pixel(image, i, j)
            p2 = get_pixel(image, i, j + 1)
            p3 = get_pixel(image, i + 1, j)
            p4 = get_pixel(image, i + 1, j + 1)

            # Color Saturation by RGB channel
            red = (p1[0] + p2[0] + p3[0] + p4[0]) / 4
            green = (p1[1] + p2[1] + p3[1] + p4[1]) / 4
            blue = (p1[2] + p2[2] + p3[2] + p4[2]) / 4

            # Results by channel
            r = [0, 0, 0, 0]
            g = [0, 0, 0, 0]
            b = [0, 0, 0, 0]

            # Get Quadrant Color
            for x in range(0, 4):
                r[x] = get_saturation(red, x)
                g[x] = get_saturation(green, x)
                b[x] = get_saturation(blue, x)

            # Set Dithered Colors
            pixels[i, j] = (r[0], g[0], b[0])
            pixels[i, j + 1] = (r[1], g[1], b[1])
            pixels[i + 1, j] = (r[2], g[2], b[2])
            pixels[i + 1, j + 1] = (r[3], g[3], b[3])

        # Return new image
        return new


# Get the pixel from the given image
def get_pixel(image, i, j):
    # Inside image bounds?
    img_width, img_height = image.size
    if i > img_width or j > img_height:
        return None

    # Get Pixel
    pixel = image.getpixel((i, j))
    return pixel


# Create a new image with the given size
def create_image(i, j):
    image = Image.new("RGB", (i, j), "white")
    return image


# Create a Half-tone version of the image
def convert_halftoning(image):
    # Get size
    img_width, img_height = image.size

    # Create new Image and a Pixel Map
    new = create_image(img_width, img_height)
    pixels = new.load()

    # Transform to half tones
    for i in range(0, img_width, 2):
        for j in range(0, img_height, 2):
            # Get Pixels
            p1 = get_pixel(image, i, j)
            p2 = get_pixel(image, i, j + 1)
            p3 = get_pixel(image, i + 1, j)
            p4 = get_pixel(image, i + 1, j + 1)

            # Transform to grayscale
            gray1 = (p1[0] * 0.299) + (p1[1] * 0.587) + (p1[2] * 0.114)
            gray2 = (p2[0] * 0.299) + (p2[1] * 0.587) + (p2[2] * 0.114)
            gray3 = (p3[0] * 0.299) + (p3[1] * 0.587) + (p3[2] * 0.114)
            gray4 = (p4[0] * 0.299) + (p4[1] * 0.587) + (p4[2] * 0.114)

            # Saturation Percentage
            sat = (gray1 + gray2 + gray3 + gray4) / 4

            # Draw white/black depending on saturation
            if sat > 223:
                pixels[i, j] = (255, 255, 255)  # White
                pixels[i, j + 1] = (255, 255, 255)  # White
                pixels[i + 1, j] = (255, 255, 255)  # White
                pixels[i + 1, j + 1] = (255, 255, 255)  # White
            elif sat > 159:
                pixels[i, j] = (255, 255, 255)  # White
                pixels[i, j + 1] = (0, 0, 0)  # Black
                pixels[i + 1, j] = (255, 255, 255)  # White
                pixels[i + 1, j + 1] = (255, 255, 255)  # White
            elif sat > 95:
                pixels[i, j] = (255, 255, 255)  # White
                pixels[i, j + 1] = (0, 0, 0)  # Black
                pixels[i + 1, j] = (0, 0, 0)  # Black
                pixels[i + 1, j + 1] = (255, 255, 255)  # White
            elif sat > 32:
                pixels[i, j] = (0, 0, 0)  # Black
                pixels[i, j + 1] = (255, 255, 255)  # White
                pixels[i + 1, j] = (0, 0, 0)  # Black
                pixels[i + 1, j + 1] = (0, 0, 0)  # Black
            else:
                pixels[i, j] = (0, 0, 0)  # Black
                pixels[i, j + 1] = (0, 0, 0)  # Black
                pixels[i + 1, j] = (0, 0, 0)  # Black
                pixels[i + 1, j + 1] = (0, 0, 0)  # Black

        # Return new image
        return new


def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    inv_gamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** inv_gamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using the lookup table
    return Image.fromarray(
        cv2.cvtColor(cv2.LUT(numpy.array(image), table), cv2.COLOR_BGR2RGB)
    )


def fake_light(image, tilesize=50):
    image_width, image_height = image.size
    for x in xrange(0, image_width, tilesize):
        for y in xrange(0, image_height, tilesize):
            br = int(255 * (1 - x / float(image_width) * y / float(image_height)))
            tile = Image.new("RGBA", (tilesize, tilesize), (255, 255, 255, 128))
            image.paste((br, br, br), (x, y, x + tilesize, y + tilesize), mask=tile)


def get_coeffs():
    global width, height
    num_of_coeff_variants = 3
    coeff_id = randint(0, num_of_coeff_variants)
    shift = randint(1, 6)
    if coeff_id == 0:
        """
        Transform right side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(0, 0), (width + shift, -shift), (width + shift, height + shift), (0, height)])
    elif coeff_id == 1:
        """
        Transform left side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(-shift, -shift), (width, 0), (width, height), (-shift, height + shift)])
    elif coeff_id == 2:
        """
        Transform top side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(-shift, -shift), (width + shift * 2, -shift), (width, height), (0, height)])
    else:
        """
        No transform
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(0, 0), (width, 0), (width, height), (0, height)])
    return coeffs


print("Start images transformation ...")
print(" - source    ", src)
print(" - background", bg)

lock = Lock()

for file_name in os.listdir(bg):
    if file_name.endswith(".jpg") or file_name.endswith(".jpeg"):
        lock.acquire()
        try:
            file_name_full = os.path.join(bg, file_name)
            img = Image.open(file_name_full).convert("LA")
            bg_images.append(img)
        finally:
            lock.release()
    else:
        print("File is not JPEG image:", file_name)
        continue

img_current_count = 0
img_total = len(os.listdir(src)) * num
bg_margin = 4
for file_name in os.listdir(src):
    if file_name.endswith(".jpg") or file_name.endswith(".jpeg"):
        for i in range(0, num):
            lock.acquire()
            try:
                img = Image.open(os.path.join(src, file_name)).convert("RGBA")
                img = img.resize((width, height), Image.ANTIALIAS)

                bg_img = bg_images[randint(0, len(bg) - 1)]
                bg_img = bg_img.resize((width + bg_margin, height + bg_margin), Image.ANTIALIAS)

                img = img.transform(
                    (width, height),
                    Image.PERSPECTIVE,
                    get_coeffs(),
                    Image.BICUBIC
                )

                bg_width, bg_height = bg_img.size

                """
                Center the image
                """
                x1 = int(math.floor((bg_width - width) / 2))
                y1 = int(math.floor((bg_height - height) / 2))

                bg_img.paste(img, (x1, y1), img)
                bg_img = bg_img.convert('RGB')

                img_current_count += 1
                if img_current_count % 10 == 0:
                    fake_light(bg_img, randint(1, 10))

                bg_img = adjust_gamma(bg_img, random.uniform(0.3, 1))

                file_name_new = str(int(round(time.time() * 1000))) + ".jpg"
                file_name_full = os.path.join(dst, file_name_new)
                bg_img.save(
                    file_name_full,
                    "JPEG", quality=100, optimize=True, progressive=True
                )

                print("Image", img_current_count, "of", img_total, "processed")
            finally:
                lock.release()
    else:
        print("File is not JPEG image:", file_name)
        continue

print("Images transformed")
