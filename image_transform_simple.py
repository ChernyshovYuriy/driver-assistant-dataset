from random import randint

import numpy
from PIL import Image


def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0] * p1[0], -p2[0] * p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1] * p1[0], -p2[1] * p1[1]])

    A = numpy.matrix(matrix, dtype=numpy.float)
    B = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(A.T * A) * A.T, B)
    return numpy.array(res).reshape(8)


img = Image.open("/home/yurii/dev/OpenCVProject/speed_limit_training/positive_samples/003.jpg")
img_width, img_height = img.size

width = 34
height = 50

img = img.resize((width, height), Image.ANTIALIAS)

num_of_coeff_variants = 3
coeff_id = randint(0, num_of_coeff_variants)
shift = randint(1, 6)

if coeff_id == 0:
    """
    Transform right side in perspective
    """
    coeffs = find_coeffs(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(0, 0), (width + shift, -shift), (width + shift, height + shift), (0, height)])
elif coeff_id == 1:
    """
    Transform left side in perspective
    """
    coeffs = find_coeffs(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(-shift, -shift), (width, 0), (width, height), (-shift, height + shift)])
elif coeff_id == 2:
    """
    Transform top side in perspective
    """
    coeffs = find_coeffs(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(-shift, -shift), (width + shift * 2, -shift), (width, height), (0, height)])
else:
    """
    No transform
    """
    coeffs = find_coeffs(
        [(0, 0), (width, 0), (width, height), (0, height)],
        [(0, 0), (width, 0), (width, height), (0, height)])

img = img.transform((width, height), Image.PERSPECTIVE, coeffs, Image.BICUBIC)
img.show()
